﻿```text
FOR zsh
alias d="docker"
alias dc="docker compose"
alias kc="kubectl"
alias kc-alpine="kubectl run -i --tty alpine --image=alpine --restart=Never -- sh"
alias mc="minikube"
alias docker-local-repo="eval $(minikube -p minikube docker-env)"
alias mc-start="minikube start --extra-config=apiserver.service-node-port-range=80-32767 --ports=8000-8081:8000-8081,80:80 --addons=ingress"

FOR PowerShell
function RUN_kc-alpine { kubectl run -i --tty alpine --image=alpine --restart=Never -- sh }
function RUN_Mc-Start { minikube start --extra-config=apiserver.service-node-port-range=80-32767 --ports=8000-8081:8000-8081,80:80 --addons=ingress }

New-Alias -Name dc -Value docker-compose
New-Alias -Name d -Value docker
New-Alias -Name kc -Value kubectl
New-Alias -Name kc-alpine -Value RUN_kc-alpine
New-Alias -Name mc -Value minikube
New-Alias -Name mc-start -Value RUN_Mc-Start
```
- start minikube with command
  - add ingress addons
  - open cluster port 8000~8081 & 80
  - accept port range from 80 to 32767
```text
minikube start --extra-config=apiserver.service-node-port-range=80-32767 --ports=8000-8081:8000-8081,80:80 --addons=ingress
```