﻿using Microsoft.AspNetCore.Mvc;

namespace Template_ViteVue3_.NetCore7.Controllers;

public class HealthController : Controller
{
    // GET
    public string Index()
    {
        Console.WriteLine("Health Check OK!");
        return "OK";
    }
}